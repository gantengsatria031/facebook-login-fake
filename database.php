<?php 
    class database
    {
        private $db_connect;
        private $cursor;
        private $sql;
        
        public function __construct()
        {
            $this->db_connect=new PDO('mysql:host=localhost; dbname=hack','hack','123');
        }
        
        public function execute($option=array())
        {
            $this->cursor=$this->db_connect->prepare($this->sql);
            if($option)
            {
                for($i=0;$i<count($option);$i++)
                {
                    $this->cursor->bindParam($i+1,$option[$i]);
                }
            }
            return $this->cursor->execute();

        }
        
        public function setQuery($sql)
        {
            $this->sql=$sql;
        }
    }

?>